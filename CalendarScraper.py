#!/usr/bin/env python

from bs4 import BeautifulSoup
from datetime import datetime
import requests
import pytz
import Ical

# Generating ICAL
UID=0
cal = Ical.build_calendar()

# Downloading contents of the web page
url = 'https://www.hs-aalen.de/semesters/20'
data = requests.get(url).text

# Creating BeautifulSoup object
soup = BeautifulSoup(data, 'html5lib')

# Get table
table = soup.find('table')

#Get all rows from table
for row in table.tbody.find_all('tr'):
    #Get all columns from each row
    columns = row.find_all('td')
    if columns != []:
        SUMMARY = columns[0].text.strip() #TITLE

        DTSTART = datetime(2000,1,1)
        DTEND = datetime(2000,1,1)
        dt = columns[1].text.strip()    #TIME

        #Get Start and End
        try:
            #Cut Days
            for sub in ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]:
                dt = dt.replace(sub + '., ', '')
            #If Date is a Range get Start and End
            if "-" in dt:
                dts = dt.split(" - ")[0]
                DTSTART = datetime(int(dts.split(".")[2]), int(dts.split(".")[1]), int(dts.split(".")[0]),0,0,0,tzinfo=pytz.timezone('Europe/Berlin'))
                dte = dt.split(" - ")[1]
                DTEND = datetime(int(dte.split(".")[2]), int(dte.split(".")[1]), int(dte.split(".")[0]),23,59,0,tzinfo=pytz.timezone('Europe/Berlin'))
            #If Date is a Date get Date
            else:
                DTSTART = datetime(int(dt.split(".")[2]), int(dt.split(".")[1]), int(dt.split(".")[0]),0,0,0,tzinfo=pytz.timezone('Europe/Berlin'))
                DTEND = DTSTART
        except:
            pass

        DESCRIPTION = columns[2].text.strip() #COMMENTS
        UID+=1
        print(SUMMARY, DESCRIPTION, DTSTART, DTEND, UID)
        event = Ical.build_event(SUMMARY, DESCRIPTION, DTSTART, DTEND, UID)
        cal.add_component(event)

Ical.write(cal)
