#!/usr/bin/env python

from icalendar import Calendar, Event
from datetime import datetime, timezone
import pytz

tz = 'Europe/Berlin'
version = "2.0"
propid = "-//PyCal//Dam0k1es//DE"
calscale = "GREGORIAN"
ics = 'Termine.ics'

def build_calendar():
    cal = Calendar()
    cal.add("VERSION", version)
    cal.add("PROPID", propid)
    cal.add("CALSCALE", calscale)
    cal.add("TZID", tz)
    return cal

def build_event(summary, description, start, end, uid):
    event = Event()
    event['uid'] =  uid
    event.add('summary', summary)
    event.add('description', description)
    event.add('dtstart', start)
    event.add('dtend', end)
    event.add('dtstamp', datetime.now(pytz.timezone(tz)))
    return event

def write(cal):
    f = open(ics, 'ab')
    f.write(cal.to_ical())
    f.close()

#e = build_event('Test', 'This is a Test', datetime(2021, 4, 4, 8, 0, 0, tzinfo=pytz.timezone(tz)), datetime(2021, 4, 4, 20, 0, 0, tzinfo=pytz.timezone(tz)))
#cal.add_component(e)
